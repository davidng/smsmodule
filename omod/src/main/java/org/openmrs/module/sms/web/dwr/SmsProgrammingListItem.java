/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.web.dwr;

import org.openmrs.Cohort;
import org.openmrs.api.CohortService;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.model.SmsProgramming;

/**
 * @author crecabarren
 *
 */
public class SmsProgrammingListItem {
	
	private Integer id;
	private String name;
	private String text;
	private Integer delay;
	private String event;
	private String cohort;

	private String sql;
	/**
	 * Empty Constructor.
	 */
	public SmsProgrammingListItem(){}
	
	/**
	 * @param programming
	 */
	public SmsProgrammingListItem(SmsProgramming programming) {
		this.id = programming.getId();
		this.name = programming.getName();
		this.text = programming.getText();
		this.delay = programming.getDelay();
		this.event = programming.getEvent();
		this.cohort = getCohortNameFromId(programming.getCohort());
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the delay
	 */
	public Integer getDelay() {
		return delay;
	}
	/**
	 * @param delay the delay to set
	 */
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
	/**
	 * @return the event
	 */
	public String getEvent() {
		return event;
	}
	/**
	 * @param event the event to set
	 */
	public void setEvent(String event) {
		this.event = event;
	}

	/**
	 * @return the cohort
	 */
	public String getCohort() {
		return cohort;
	}

	/**
	 * @param cohort the cohort to set
	 */
	public void setCohort(String cohort) {
		this.cohort = cohort;
	}
	
	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
	
	
	private String getCohortNameFromId(Integer cohortId){
		CohortService cs = Context.getCohortService();
		Cohort cohort = cs.getCohort(cohortId);
		if(cohort != null){
			return cohort.getName();
		}else{
			return "Cohort " + String.valueOf(cohortId);
		}
	}
}

package org.openmrs.module.sms.web.dwr;

import java.sql.Timestamp;

import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.model.SmsQueue;

public class SmsQueueListItem {
	private Integer id;// Sms queue id 
	private Integer patientID;// patient id
	private String smsName; // sms name
	private String patientName;// patient name
	private Integer noOfAttempt;// number of attempt
	private Timestamp lastAttemptDateTime;// last attempt date time
	
	public SmsQueueListItem(){}
	// constructor
	public SmsQueueListItem(SmsQueue queue){
		this.id = queue.getId();
		this.patientID = queue.getPatientId();
		this.smsName = Context.getService(SmsService.class).getMessageById(queue.getProgrammingId()).getName();
		Patient patient = Context.getPatientService().getPatient(queue.getPatientId());
		this.patientName = patient.getFamilyName() + " " + patient.getGivenName();
		this.noOfAttempt = queue.getNumberAttempt();
		this.lastAttemptDateTime = queue.getLastAttemptDateTime();
	}
	
	// Begin Getters and Setters
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPatientID() {
		return patientID;
	}
	public void setPatientID(Integer patientID) {
		this.patientID = patientID;
	}
	public Integer getNoOfAttempt() {
		return noOfAttempt;
	}
	public void setNoOfAttempt(Integer noOfAttempt) {
		this.noOfAttempt = noOfAttempt;
	}

	public Timestamp getLastAttemptDateTime() {
		return lastAttemptDateTime;
	}

	public void setLastAttemptDateTime(Timestamp lastAttemptDateTime) {
		this.lastAttemptDateTime = lastAttemptDateTime;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getSmsName() {
		return smsName;
	}
	public void setSmsName(String smsName) {
		this.smsName = smsName;
	}

}

/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
function initStores() {
	var gatewayStore = new Ext.data.Store({
		storeId : "gateway-store",
		proxy : new Ext.ux.data.DwrProxy({
			apiActionToHandlerMap : {
				create : {
					dwrFunction : DWRSmsProviderService.saveOrUpdate
				},
				read : {
					dwrFunction : DWRSmsProviderService.getProviders
				},
				update : {
					dwrFunction : DWRSmsProviderService.saveOrUpdate,
					getDwrArgsFunction: function(request, recordDataArray, oldRecordDataArray) {
						return [recordDataArray, oldRecordDataArray];
					}
				},
				destroy : {
					dwrFunction : DWRSmsProviderService.deleteProvider
				}
			}
		}),
		reader : new Ext.data.JsonReader({
			idProperty : "id",
			fields :[
			         {name : "id"},
			         {name : "name"},
			         {name : "address"},
			         {name : "username"},
			         {name : "password"},
			         {name : "preferred"},
			         {name : "voided"}
			] 
		}),
		writer : new Ext.data.JsonWriter(),
		autoLoad : true,
		autoSave : true,
		listeners : {
			add : function(store, records, index){
				try{
					Ext.getCmp("gateway-form-name").disable();
					Ext.getCmp("gateway-form-username").disable();
					Ext.getCmp("gateway-form-password").disable();
					Ext.getCmp("gateway-form-address").disable();
					Ext.getCmp("gateway-form-preferred").disable();
					Ext.getCmp("gateway-form-voided").disable();
				} catch(e){}
			},
			remove : function(store, record, index){
				try{
					Ext.getCmp("gateway-form-name").disable();
					Ext.getCmp("gateway-form-username").disable();
					Ext.getCmp("gateway-form-password").disable();
					Ext.getCmp("gateway-form-address").disable();
					Ext.getCmp("gateway-form-preferred").disable();
					Ext.getCmp("gateway-form-voided").disable();
				}catch(e){}
			},
			save : function(store, data){
				store.reload();
			}
		}
	});
	
	var messagesStore = new Ext.data.GroupingStore({
		storeId : "message-store",
		proxy : new Ext.ux.data.DwrProxy({
			apiActionToHandlerMap : {
				create : {
					dwrFunction : DWRSmsProgrammingService.saveOrUpdate,
					getDwrArgsFunction: function(request, recordDataArray) {
						return [
						        Ext.getCmp("message-form-cohort").getValue(),
								Ext.getCmp('message-form-event-id').getValue(),
						        recordDataArray
						];
					}
				},
				read : {
					dwrFunction : DWRSmsProgrammingService.getMessages
				},
				update : {
					dwrFunction : DWRSmsProgrammingService.saveOrUpdate,
					getDwrArgsFunction: function(request, recordDataArray, oldRecordDataArray) {
						return [
						        Ext.getCmp("message-form-cohort").getValue(),
						        Ext.getCmp('message-form-event-id').getValue(),
						        recordDataArray,
						        oldRecordDataArray
						];
					}
				},
				destroy : {
					dwrFunction : DWRSmsProgrammingService.deleteMessage
				}
			}
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			fields : [
			          {name : 'id'},
			          {name : 'name'},
			          {name : 'text'},
			          {name : 'delay'},
			          {name : 'event'},
			          {name : 'cohort'}
			]
		}),
		writer : new Ext.data.JsonWriter(),
		autoLoad : true,
		autoSave : true,
		listeners : {
			add : function(store, records, index){
				try{
					Ext.getCmp("message-form-name").disable();
					Ext.getCmp("message-form-text").disable();
					Ext.getCmp("message-form-delay").disable();
					Ext.getCmp("message-form-event").disable();
				}catch(e){}
			},
			remove : function(store, records, index){
				try{
					Ext.getCmp("message-form-name").disable();
					Ext.getCmp("message-form-text").disable();
					Ext.getCmp("message-form-delay").disable();
					Ext.getCmp("message-form-event").disable();
				}catch(e){}
			},
			save : function(store, data){
				store.reload();
			}
		}
	});
	
	var cohortStore = new Ext.data.Store({
		storeId : 'cohort-store',
		proxy : new Ext.ux.data.DwrProxy({
			apiActionToHandlerMap : {
				read : {dwrFunction : DWRCohortDefinitionService.getCohorts}
			}
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			fields : [
				{name : "id"},
				{name : "name"},
				{name : "description"}
			]
		}),
		autoLoad : true,
		autoSave : true
	});	
	
	var queueStore = new Ext.data.Store({
		storeId : "message-queue-store",
		proxy : new Ext.ux.data.DwrProxy({
			apiActionToHandlerMap : {
				read : {dwrFunction : DWRSmsMessageQueueService.getSmsQueues},
				destroy : {
					dwrFunction : DWRSmsMessageQueueService.deleteMessageFromQueue
				}
			}
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			fields : [
			         {name : "id"},
			         {name : "patientID"},
			         {name: "smsName"},
			         {name : "patientName"},
			         {name : "noOfAttempt"},
			         {name : "lastAttemptDateTime"}
			]
		}),
		writer : new Ext.data.JsonWriter(),
		autoLoad : true,
		autoSave : true,
		listeners : {
			save : function(store, data){
				store.reload();
			}
		}
	});	
	
	// The data store holding the states; shared by each of the ComboBox examples below
    var patientStore = new Ext.data.ArrayStore({
    	storeId : 'patient-store',
    	autoLoad: false,
    	autoSave: true,
    	autoDestroy: true,
    	data: [[]],
			fields : [
				{name : "patientId"},
				{name : "personName"},
				{name : "displayName"}
			]
    });
    
	
//	var store = new Ext.data.Store({
//		storeId : 'storeId',
//		proxy : new Ext.ux.data.DwrProxy({
//			apiActionToHandlerMap : {
//				create : {
//					dwrFunction : DWR__Service.__method__ 
//				},
//				read : {
//					dwrFunction : DWR__Service.__method__
//				},
//				update : {
//					dwrFunction : DWR__Service.__method__,
//					getDwrArgsFunction: function(request, recordDataArray, oldRecordDataArray) {
//						return [recordDataArray, oldRecordDataArray];
//					}
//				},
//				destroy : {
//					dwrFunction : DWR__Service.__method__
//				}
//			}
//		}),
//		reader : new Ext.data.JsonReader({
//			idProperty : 'id',
//			fields : []
//		}),
//		writer : new Ext.data.JsonWriter(),
//		autoLoad : true,
//		autoSave : true,
//		listeners : {
//			add : function(store, records, index){
//			},
//			remove : function(store, records, index){
//			},
//			save : function(store, data){
//			}
//		}
//	});
}

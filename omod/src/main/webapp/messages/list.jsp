<%--
Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.

Reproduction or distribution of this source code is prohibited.
--%>
<%@ include file="/WEB-INF/template/include.jsp" %>

<script>
Ext.ns('Extensive.grid');

Extensive.grid.ItemDeleter = Ext.extend(Ext.grid.RowSelectionModel, {
    width: 55,
    
    sortable: false,
	dataIndex: 0, // this is needed, otherwise there will be an error
    
    menuDisabled: true,
    fixed: true,
    id: 'deleter',
    
    initEvents: function(){
        Extensive.grid.ItemDeleter.superclass.initEvents.call(this);
    },
    
    renderer: function(v, p, record, rowIndex){
        return '<div class="extensive-remove" style="width: 25px; height: 16px;"><img src="../../moduleResources/sms/delete.png" alt="Delete" /></div>';
    }
});
	
var itemDeleter = new Extensive.grid.ItemDeleter();

var editor = new Ext.ux.grid.RowEditor({
    saveText: 'Update',
});

function makeMessageListPanel(){
	var panel = new Ext.grid.GridPanel({
		store : Ext.StoreMgr.lookup('message-store'),
        width: 600,
        region:'center',
        margins: '0 5 5 5',
        autoExpandColumn: 'name',
        plugins: [editor],
        view: new Ext.grid.GroupingView({
            markDirty: false
        }),

        columns: [
        new Ext.grid.RowNumberer(),
        {
        	id : 'id', 
        	header : '<spring:message javaScriptEscape="true" code="sms.id" />', 
        	dataIndex : 'id', 
        	width:50
       	},
       	{
       		id : 'name', 
       		header : '<spring:message javaScriptEscape="true" code="sms.name" />', 
       		dataIndex : 'name', 
       		width:150,
       		editor: {
        		xtype: 'textfield', 
        		allowBlank: true 
       		}
       	},
       	{
       		id : 'delay', 
       		header : '<spring:message javaScriptEscape="true" code="sms.days.after" />', 
       		dataIndex : 'delay', 
       		width:100,
       		editor: {
        		xtype: 'textfield', 
        		allowBlank: true 
       		}
       	},
       	{
       		id : 'event', 
       		header : '<spring:message javaScriptEscape="true" code="sms.event" />', 
       		dataIndex : 'event', 
       		width:200,
       		editor: {
        		xtype: 'textfield', 
        		allowBlank: true 
       		}
       	},
       	{
       		id : 'cohort',
       		header : '<spring:message javaScriptEscape="true" code="sms.criteria" />', 
       		dataIndex : 'cohort',
       		width:200
     	},
     	{
     		id : 'text', 
     		header : '<spring:message javaScriptEscape="true" code="sms.text" />', 
     		dataIndex : 'text',
     		width:200,
     		editor: {
        		xtype: 'textfield', 
        		allowBlank: true 
       		}
     	},itemDeleter]
    });
	
	panel.on('cellclick', function(grid, rowIndex, columnIndex, e){
		if(columnIndex==grid.getColumnModel().getIndexById('deleter')) {
			if(confirm("Do you want to delete this record?")){
				var store = grid.getStore();
				var record = store.getAt(rowIndex);
				var id = record.id;
				store.remove(record);
				store.reload();
				store.on('load', function(store, records, successful, operation, eOpts) {
					var newRecord = store.getAt(rowIndex);
					if(newRecord)
						if(newRecord.id == id){
							id = -1;
							alert("Could not delete the item");
						}
				}, this, {single: true, delay: 200});
			}
		}
    });
	
	return panel;
}
</script>
/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import org.openmrs.BaseOpenmrsObject;

/**
 * @author crecabarren
 *
 */
public class SmsHistory extends BaseOpenmrsObject implements Serializable {
	
	private static final long serialVersionUID = 2387428725687432L;
	private Integer id;
	private Integer programmingId;
	private Integer patientId;
	private Integer providerId;
	private Timestamp sentToGatewayDate;
	private String sentToGatewayResult;
	private String smsSentResult;
	private Timestamp smsSentDate;
	private String broadcastId;
	private SmsHistory history;
	private String smsSentResultCode;
	private Integer obsId;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the programmingId
	 */
	public Integer getProgrammingId() {
		return programmingId;
	}
	/**
	 * @param programmingId the programmingId to set
	 */
	public void setProgrammingId(Integer programmingId) {
		this.programmingId = programmingId;
	}
	/**
	 * @return the patientId
	 */
	public Integer getPatientId() {
		return patientId;
	}
	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}
	/**
	 * @return the providerId
	 */
	public Integer getProviderId() {
		return providerId;
	}
	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(Integer providerId) {
		this.providerId = providerId;
	}
	/**
	 * @return the sentToGatewayDate
	 */
	public Timestamp getSentToGatewayDate() {
		return sentToGatewayDate;
	}
	/**
	 * @param sentToGatewayDate the sentToGatewayDate to set
	 */
	public void setSentToGatewayDate(Timestamp sentToGatewayDate) {
		this.sentToGatewayDate = sentToGatewayDate;
	}
	/**
	 * @return the sentToGatewayResult
	 */
	public String getSentToGatewayResult() {
		return sentToGatewayResult;
	}
	/**
	 * @param sentToGatewayResult the sentToGatewayResult to set
	 */
	public void setSentToGatewayResult(String sentToGatewayResult) {
		this.sentToGatewayResult = sentToGatewayResult;
	}
	/**
	 * @return the smsSentResult
	 */
	public String getSmsSentResult() {
		return smsSentResult;
	}
	/**
	 * @param smsSentResult the smsSentResult to set
	 */
	public void setSmsSentResult(String smsSentResult) {
		this.smsSentResult = smsSentResult;
	}
	/**
	 * @return the smsSentDate
	 */
	public Timestamp getSmsSentDate() {
		return smsSentDate;
	}
	/**
	 * @param smsSentDate the smsSentDate to set
	 */
	public void setSmsSentDate(Timestamp smsSentDate) {
		this.smsSentDate = smsSentDate;
	}
	/**
	 * @return the broadcastId
	 */
	public String getBroadcastId() {
		return broadcastId;
	}
	/**
	 * @param broadcastId the broadcastId to set
	 */
	public void setBroadcastId(String broadcastId) {
		this.broadcastId = broadcastId;
	}
	
	public SmsHistory getHistory(Integer patientId, SmsProgramming programming) {
		return history;
	}
	public String getSmsSentResultCode() {
		return smsSentResultCode;
	}
	public void setSmsSentResultCode(String smsSentResultCode) {
		this.smsSentResultCode = smsSentResultCode;
	}
	/**
	 * @return the obs_id
	 */
	public Integer getObsId() {
		return obsId;
	}
	/**
	 * @param obs_id the obs_id to set
	 */
	public void setObsId(Integer obsId) {
		this.obsId = obsId;
	}
}


/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */

package org.openmrs.module.sms.api.db;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.openmrs.Patient;
import org.openmrs.module.sms.model.SmsHistory;
import org.openmrs.module.sms.model.SmsProgramming;
import org.openmrs.module.sms.model.SmsProvider;
import org.openmrs.module.sms.model.SmsQueue;

/**
 * @author crecabarren
 *
 */
public interface SmsModuleDAO {
	
	/**
	 * 
	 */
	public void setSessionFactory(SessionFactory session);
		
	/**
	 * @see org.openmrs.module.sms.api.SmsService#createProvider(org.openmrs.module.sms.model.SmsProvider)
	 */
	public SmsProvider createProvider(SmsProvider provider);
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#deleteProvider(org.openmrs.module.sms.model.SmsProvider)
	 */
	public void deleteProvider(SmsProvider provider);
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getAllProviders()
	 */
	public List<SmsProvider> getAllProviders();
	
	
	public List<SmsProvider> getActiveProviders();
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getPreferredProvider()
	 */
	public List<SmsProvider> getPreferredProvider();
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getProviderById(java.lang.String)
	 */
	public SmsProvider getProviderById(Integer id);
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getAllMessages()
	 */
	public List<SmsProgramming> getAllMessages();
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getMessageById(Integer)
	 */
	public SmsProgramming getMessageById(Integer id);
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getMessagesByKeyWord(String)
	 */
	public List<SmsProgramming> getMessagesByKeyWord(String key);
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#createMessage(SmsProgramming)
	 */
	public SmsProgramming createMessage(SmsProgramming programming);
	
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#deteleMessage(SmsProgramming)
	 */
	public void deteleMessage(SmsProgramming programming);
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#createHistory(SmsHistory)
	 */
	public void createHistory(SmsHistory history);

	public SmsHistory getHistory(Patient patient, SmsProgramming programming);

	public List<SmsHistory> getListHistory(Patient patient, SmsProgramming programming);
        
    public List<SmsHistory> getListHistory(SmsProgramming programming);
	
	public List<Integer> getListObservationFromConceptAndPatient(Patient patient, SmsProgramming programming);
        
    public List getListObservationFromConcept(SmsProgramming programming);
    
    public List getListObservationFromProgramming(SmsProgramming programming, List<Integer> listPatient, boolean sendRepeat);

	public Date getDateSQL(String sql, Integer patientId);	
	
	public List<SmsQueue> getAllSmsQueue();
	
	public void saveSmsQueue(SmsQueue smsQueue);
	
	public void deleteSmsQueue(SmsQueue smsQueue);
	
	public SmsQueue getSmsQueueById(Integer id);
	
	public void deleteAllSmsQueue();
}
